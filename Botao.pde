class Botao
{
  public static final int ID_BOTAO_NOVO = 0;
  public static final int ID_BOTAO_TRANSFORMAR = 1;
  public static final int ID_BOTAO_DELETAR = 2;
  public static final int ID_BOTAO_SELECIONAR = 3;
  public static final int ID_BOTAO_COLORIR = 8;
  
  public static final int ID_BOTAO_CIRCULO = 4;
  public static final int ID_BOTAO_REGULARPOLY = 5;
  public static final int ID_BOTAO_POLY = 6;
  public static final int ID_BOTAO_LINHA = 7;
  
  public static final int ID_BOTAO_VERMELHO = 9; //255, 0 ,0
  public static final int ID_BOTAO_VERDE = 10; //0, 255, 0
  public static final int ID_BOTAO_AZUL = 11; //0, 0 , 255
  public static final int ID_BOTAO_AMARELO = 12; //250,250,0
  public static final int ID_BOTAO_VERDEAGUA = 13; // 0,250,250
  public static final int ID_BOTAO_MARROM = 14;// 85,34,0
  public static final int ID_BOTAO_ROSA = 15;//255,0,255
  public static final int ID_BOTAO_LARANJA = 16;//255,120,0
  public static final int ID_BOTAO_BRANCO = 17;//250,250,250
  public static final int ID_BOTAO_CINZA = 18;//125,125,125
  public static final int ID_BOTAO_PRETO = 19;//0,0,0
  public static final int ID_BOTAO_MAGENTA = 21;//139,0,139
  public static final int ID_BOTAO_SLATEBLUE = 22;//106,90,205
  public static final int ID_BOTAO_DARKCYAN = 23;//0,139,139
  public static final int ID_BOTAO_HOTPINK = 24;//255,105,180
  public static final int ID_BOTAO_SPRINGGREENS = 25;//0,255,127
  
  public static final int ID_BOTAO_CORSELECIONADA = 20;
  
  
  float menorX;
  float menorY;
  float maiorX;
  float maiorY;
  
  int id;
  boolean visivel;
  boolean selecionado;
  ArrayList<Vertice> vertices = new ArrayList<Vertice>();
  Vertice posicao;
  Vertice pivot;
  float largura;
  float altura;
  String texto;
  Cor corAtual;
  Cor corFundo;
  Cor corTexto;
  Cor corBorda;
  Cor corBordaAtual;
  Cor corBordaSelec;
  Cor corMousePressed;
  Cor corMouseEnter;
  Cor corSelecionado;
  Utils util = new Utils();
    
  public Botao(int id, String texto, Vertice posicao,float largura, float altura,boolean visivel)
  {
      this.id = id;
      this.largura = largura;
      this.altura = altura;
      this.texto = texto;
      this.visivel = visivel;
      this.selecionado = false;
      
      corFundo = new Cor(100,100,100);
      corAtual = corFundo;
      corTexto = new Cor(0,0,0);
      corBorda = new Cor(0,0,0);
      corBordaAtual = corBorda;
      corSelecionado = new Cor(175,175,175);
      corMousePressed = new Cor(200, 200, 200);
      corMouseEnter = new Cor(150,150,150);
      
      pivot = new Vertice(posicao.x + (largura/2), posicao.y + (altura/2));
      
      vertices.add(new Vertice(posicao.x, posicao.y));
      vertices.add(new Vertice(posicao.x + largura, posicao.y));
      vertices.add(new Vertice(posicao.x + largura, posicao.y + altura));
      vertices.add(new Vertice(posicao.x, posicao.y + altura));
      
      
      verificacaoAreaVertices();
  }
  
  public Botao(int id, String texto, Vertice posicao,float largura, float altura,boolean visivel, Cor corFundo)
  {
      corFundo.corIdBotao(id);
    
      this.id = id;
      this.largura = largura;
      this.altura = altura;
      this.texto = texto;
      this.visivel = visivel;
      this.selecionado = false;
      
      this.corFundo = corFundo;
      corAtual = corFundo;
      corTexto = new Cor(0,0,0);
      corBorda = new Cor(0,0,0);
      corBordaSelec = new Cor(218, 165, 32);
      corBordaAtual = corBorda;
      corSelecionado = corFundo;
      corMousePressed = corFundo;
      corMouseEnter = corFundo;
      
      pivot = new Vertice(posicao.x + (largura/2), posicao.y + (altura/2));
      
      vertices.add(new Vertice(posicao.x, posicao.y));
      vertices.add(new Vertice(posicao.x + largura, posicao.y));
      vertices.add(new Vertice(posicao.x + largura, posicao.y + altura));
      vertices.add(new Vertice(posicao.x, posicao.y + altura));
      
      
      verificacaoAreaVertices();
  }
  
  public void verificacaoAreaVertices()
  {
    maiorX = vertices.get(0).x;
    menorX = vertices.get(0).x;
    maiorY = vertices.get(0).y;
    menorY = vertices.get(0).y;
    
    for(int i = 1; i < vertices.size(); i++)
    {
      if(maiorX < vertices.get(i).x)
        maiorX = vertices.get(i).x;
        
      if(menorX > vertices.get(i).x)
        menorX = vertices.get(i).x;
        
      if(maiorY < vertices.get(i).y)
        maiorY = vertices.get(i).y;
        
      if(menorY > vertices.get(i).y)
        menorY = vertices.get(i).y;
    }
  }
  
  public void setTexto(String texto)
  {
    this.texto = texto;
  }
  
  public void desenhar()
  {
    for(int i = 0; i < vertices.size(); i++)
    {
      if(i != vertices.size() - 1)
      {
        linha((int)vertices.get(i).x, (int)vertices.get(i).y, (int)vertices.get(i + 1).x, (int)vertices.get(i + 1).y, this.corBordaAtual);
      }
      else
      {
        linha((int)vertices.get(i).x, (int)vertices.get(i).y, (int)vertices.get(0).x, (int)vertices.get(0).y, this.corBordaAtual);
      }
    }
    
  }
  
  public void colorir(color c)
  {
    Vertice v = new Vertice();
    
    for(int i = (int)menorX; i <= (int)maiorX; i++)
    {
      for(int j = (int)menorY; j <= (int)menorY; j++)
      {
        v.x = i;
        v.y = j;
        
        if(util.dentroPoligono(v, vertices))
          set(i,j,c);
      }
    }
  }
  
  public boolean equals(Botao btn)
  {
    if(btn.id != this.id)
      return false;
      
      return true;
  }
  
}