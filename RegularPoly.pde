class RegularPoly extends Objeto
{
  float menorX;
  float maiorX;
  float menorY;
  float maiorY;
  
  ArrayList<Vertice> vertices;
  float raio;
  int quantidadeLados;
  
  Transform transform = new Transform();
  Utils util = new Utils();
  
  public RegularPoly(Vertice pivot, int lados, float raio)
  {
    super(pivot);
    this.raio = raio;
    vertices = new ArrayList<Vertice>();
    Vertice v = new Vertice();
    quantidadeLados = lados;
    
    for(int i = 1; i <= lados; i++)
    {
      float grausRad = ((2 * PI) / lados) * i;
      v.y = raio;
      v.x = 0;
      transform.rotacionar(v, grausRad);
      
      vertices.add(new Vertice(v.x, v.y));
    }
    
    for(int i = 0; i < vertices.size(); i++)
    {
      transform.transladar(vertices.get(i), pivot.x, pivot.y);
    }
  }
  
  void escala(float sX, float sY)
  {
    for(int i = 0; i < vertices.size(); i++)
    {
      transform.escala(vertices.get(i) , sX, sY);
    }
    
    transform.escala(pivot, sX, sY);
  }
  
  void escalaPivot(float sX, float sY)
  {
    Vertice temp  = new Vertice(this.pivot.x, this.pivot.y);
    this.transladar(-temp.x, -temp.y);
    
    for(int i = 0; i < vertices.size(); i++)
    {
      transform.escala(vertices.get(i), sX, sY);
    }
    
    this.transladar(temp.x, temp.y);
  }
  
  void transladar(float deltaX, float deltaY)
  {
    for(int i = 0; i < vertices.size(); i++)
    {
      transform.transladar(vertices.get(i), deltaX, deltaY);
    }
    
    transform.transladar(pivot, deltaX, deltaY);
  }
  
  void rotacionar(float grausRad)
  {
    for(int i = 0; i < vertices.size(); i++)
    {
      transform.rotacionar(vertices.get(i), grausRad);
    }
    
    transform.rotacionar(pivot, grausRad);
  }
  
  void rotacionarPivot(float grausRad)
  {
    Vertice temp = new Vertice(this.pivot.x, this.pivot.y);
    
    this.transladar(-temp.x, -temp.y);
    
    for(int i = 0; i < vertices.size(); i++)
    {
      transform.rotacionar(vertices.get(i), grausRad);
    }
    
    this.transladar(temp.x, temp.y);
  }
  
  void mapear(Universo u)
  {
      for(int i = 0; i < vertices.size(); i++)
      {
        u.mapeamento(vertices.get(i));
      }
      
      u.mapeamento(pivot);
  }
  
  void mapearRev(Universo u)
  {
      for(int i = 0; i < vertices.size(); i++)
      {
        u.mapeamentoReverso(vertices.get(i));
      }
      
      u.mapeamentoReverso(pivot);
  }
  
  void copiarTodosPontos(Objeto objAlvo) throws Exception
  {
    if(objAlvo instanceof RegularPoly)
    {
      RegularPoly alvo = (RegularPoly)objAlvo;
      
      if(alvo.vertices.size() == this.vertices.size())
      {
        for(int i = 0; i < alvo.vertices.size(); i++)
        {
          alvo.vertices.get(i).x = this.vertices.get(i).x;
          alvo.vertices.get(i).y = this.vertices.get(i).y;
        }
      
        alvo.pivot.x = this.pivot.x;
        alvo.pivot.y = this.pivot.y;
      }
    }
    else
    {
      throw new Exception("Não é possível copiar objetos de tipos diferentes");
    }
  }
  
  
  void desenhar()
  {
    
    for(int i = 0; i < vertices.size(); i++)
    {
      if(i != vertices.size() - 1)
      {
       linha((int)vertices.get(i).x,(int) vertices.get(i).y,(int) vertices.get(i + 1).x,(int)vertices.get(i + 1).y, corBordaAtual);  
      }
      else
      {
        linha((int)vertices.get(i).x,(int) vertices.get(i).y,(int) vertices.get(0).x,(int)vertices.get(0).y, corBordaAtual);
      }
    }
    
    linha((int)pivot.x,(int) pivot.y - 3,(int) pivot.x,(int) pivot.y +3 , corBordaAtual);
    linha((int)pivot.x - 3,(int) pivot.y,(int) pivot.x + 3,(int) pivot.y, corBordaAtual);
  }
  
  void desenhar(boolean colorir)
  {
    if(colorir)
      colorir();
      
      desenhar();
  }
  
  public boolean dentroPoligono(Vertice v)
  {
    return util.dentroPoligono(v, vertices);
  }
  
   private void verificacaoAreaVertice()
   {
    menorX = vertices.get(0).x;
    maiorX = vertices.get(0).x;
    menorY = vertices.get(0).y;
    maiorY = vertices.get(0).y;
    
    for(int i = 1; i < vertices.size(); i++)
    {
      if(vertices.get(i).x < menorX)
        menorX = vertices.get(i).x;
        
      if(vertices.get(i).x > maiorX)
        maiorX = vertices.get(i).x;
        
      if(vertices.get(i).y < menorY)
        menorY = vertices.get(i).y;
        
      if(vertices.get(i).y > maiorY)
        maiorY = vertices.get(i).y;
    }
  }
  
   public void colorir()
  {
    verificacaoAreaVertice();
    
    Vertice v = new Vertice();
    
    for(int i = (int)menorX ; i <= (int)maiorX; i++)
    {
      for(int j = (int)menorY; j <= (int)maiorY; j++)
      {
         v.x = i;
         v.y = j;
         
         if(dentroPoligono(v))
         {
           set(i, j, color(corPreenchimento.R, corPreenchimento.G, corPreenchimento.B));
         }
      }
    }
  }
  
}