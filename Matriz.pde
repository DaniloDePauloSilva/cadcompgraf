class Matriz
{
  float m[][];
  
  public Matriz(int d)
  {
    m = new float[3][3];
    
    for(int i = 0; i < d; i++)
    {
      m[i][i] = 1;
    }
    
  }
  
  public Matriz(int i, int j)
  {
    m = new float[i][j];
  }
  
  public Matriz(float a, float b, float c,float d, float e, float f,float g, float h, float i)
  {
    m = new float[3][3];
    
    m[0][0] = a; m[0][1] = b; m[0][2] = c;
    m[1][0] = d; m[1][1] = e; m[1][2] = f;
    m[2][0] = g; m[2][1] = h; m[2][2] = i;
  }
}