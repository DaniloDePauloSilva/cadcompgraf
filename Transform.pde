 class Transform
 {
   Matriz matOp = new Matriz(3,3);
   Matriz matVer = new Matriz(3,1);
   
   void rotacionar(Vertice v, float grausRad)
   {
      matrizVertice(v);
      matrizRotacao(grausRad);
      
      Matriz res = multiplicacao(matOp, matVer);
      
      v.x = res.m[0][0];
      v.y = res.m[1][0];
      
      res = null;
   }
  
  void transladar(Vertice v, float deltaX, float deltaY)
  {
    matrizVertice(v);
    matrizTranslacao(deltaX, deltaY);
    
    Matriz res = multiplicacao(matOp, matVer);
    
    v.x = res.m[0][0];
    v.y = res.m[1][0];
    
    res = null;
  }
  
  void escala(Vertice v, float sX, float sY)
  {
    matrizVertice(v);
    matrizEscala(sX, sY);
    
    Matriz res = multiplicacao(matOp, matVer);
    
    v.x = res.m[0][0];
    v.y = res.m[1][0];
    
    res = null;
  }
  
  void matrizEscala(float sX, float sY)
  {
      matOp.m[0][0] = sX;
      matOp.m[0][1] = 0;
      matOp.m[0][2] = 0;
      matOp.m[1][0] = 0;
      matOp.m[1][1] = sY;
      matOp.m[1][2] = 0;
      matOp.m[2][0] = 0;
      matOp.m[2][1] = 0;
      matOp.m[2][2] = 1;
  }
 
  void matrizTranslacao(float deltaX, float deltaY)
  {
    matOp.m[0][0] = 1;
    matOp.m[0][1] = 0;
    matOp.m[0][2] = deltaX;
    matOp.m[1][0] = 0;
    matOp.m[1][1] = 1;
    matOp.m[1][2] = deltaY;
    matOp.m[2][0] = 0;
    matOp.m[2][1] = 0;
    matOp.m[2][2] = 1;
  }
  
  void matrizRotacao(float grausRad)
  {
   
    
    matOp.m[0][0] = cos(grausRad); 
    matOp.m[0][1] = -sin(grausRad);
    matOp.m[0][2] = 0;
    matOp.m[1][0] = sin(grausRad);
    matOp.m[1][1] = cos(grausRad);
    matOp.m[1][2] = 0;
    matOp.m[2][0] = 0;
    matOp.m[2][1] = 0;
    matOp.m[2][2] = 1;
 
  }
  
  void matrizVertice(Vertice v)
  {
    matVer.m[0][0] = v.x;
    matVer.m[1][0] = v.y;
    matVer.m[2][0] = 1;
  }
  
  Matriz multiplicacao(Matriz mat1, Matriz mat2)
  {
    if(mat1.m[0].length != mat2.m.length)
    {
      return null;
    }
    
    Matriz matResult = new Matriz(mat1.m.length, mat2.m[0].length);
    
    for(int i = 0; i < mat1.m.length; i++)
    {
      for(int j = 0; j < mat2.m[0].length; j++)
      {
        for(int k = 0; k < mat2.m.length; k++)
        {
          matResult.m[i][j] += (mat1.m[i][k] * mat2.m[k][j]);
        }  
      }  
    }
    
    return matResult;
  }
  
}