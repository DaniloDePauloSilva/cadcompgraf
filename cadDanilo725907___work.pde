


Objeto objetoTemp;

Menu menu = new Menu();
Universo u;
Utils util = new Utils();

//ESTADO DO APLICATIVO------------------------
ModoEdicao MODO_EDICAO = new ModoEdicao();
int tipoCriacao;
//--------------------------------------------

Vertice vClickI;
Vertice vClickF;
Vertice vMouseAtual;

Cor corSelec;

ArrayList<Objeto> objetos = new ArrayList<Objeto>(); 
int indiceObjSel;

int qtdeLadosRegPoly = 3;
float tamRaio = 100;


boolean criando = false;

void setup()
{
  background(255);
  size(950, 600);
  
  vMouseAtual = new Vertice();
  u  = new Universo(-10, 10);
  objetoTemp = null;
  MODO_EDICAO.modoEdicao = ModoEdicao.MODO_EDICAO_CRIAR;
  indiceObjSel = -1;
  desenharTodosObjetos(true);
  
}

void draw()
{
 
  
}

void mousePressed()
{
   //System.out.println("MOUSE PRESSED: X:" + mouseX + "Y:" + mouseY);
   vClickI = new Vertice(mouseX, mouseY);
   int indiceMenu = botaoClickado(vClickI);
   println("MOUSE PRESSED indiceMenu: " + indiceMenu);
   if(indiceMenu < 0)
   {
     Editar();
   }
   else
   {
      AcaoBotao(indiceMenu);    
   }  
}

void AcaoBotao(int indiceClickado)
{
  
  println("default mouse pressed " + indiceClickado);
  switch(indiceClickado)
  {
    case Botao.ID_BOTAO_NOVO:
    alterarEstadoNOVO();
    break;
    
    case Botao.ID_BOTAO_TRANSFORMAR:
    alterarEstadoTRANSFORMAR();
    break;
    
    case Botao.ID_BOTAO_DELETAR:
    alterarEstadoDELETAR();
    break;
    
    case Botao.ID_BOTAO_SELECIONAR:
    alterarEstadoSELECIONAR();
    break;
    
    case Botao.ID_BOTAO_CIRCULO:
    alterarEstadoCIRCULO();
    break;
    
    case Botao.ID_BOTAO_REGULARPOLY:
    alterarEstadoREGULARPOLY();
    break;
    
    case Botao.ID_BOTAO_POLY:
    alterarEstadoPOLY();
    break;
    
    case Botao.ID_BOTAO_LINHA:
    alterarEstadoLINHA();
    break;
    
    case Botao.ID_BOTAO_COLORIR:
    alterarEstadoCOLORIR();
    break;
    
    default:
    
    verificaBotoesCores(indiceClickado);
    return;
    
  }
  
  menu.DesenharMousePressed(indiceClickado);
}

void verificaBotoesCores(int indiceBtn)
{
  if(menu.btnCor(indiceBtn))
  {
    Botao btnSelec = menu.retornaBotaoPorId(indiceBtn);
    corSelec = btnSelec.corAtual;
    menu.alterarCorSelecionada(corSelec);
  }
}

void Editar()
{
   switch(MODO_EDICAO.modoEdicao)
     {
       case ModoEdicao.MODO_EDICAO_TRANSFORMAR:
         break;
         
       case ModoEdicao.MODO_EDICAO_CRIAR:
       
         if(tipoCriacao == ModoEdicao.CRIAR_REGULAR_POLY)
         {
           //Vertice pivot = new Vertice(mouseX, mouseY);
           //u.mapeamentoReverso(pivot);
           //Objeto objeto = new RegularPoly(pivot, qtdeLadosRegPoly, 4);
           //objetos.add((RegularPoly)objeto);
           //objetos.get(objetos.size() - 1).mapear(u);
         
           //System.out.println("QUANTIDADE OBJETOS:" + objetos.size());
           Vertice vInicial = new Vertice(vClickI.x, vClickI.y);
           Vertice vAtual = new Vertice(mouseX, mouseY);
           u.mapeamentoReverso(vInicial);
           u.mapeamentoReverso(vAtual);
           
           if(!criando)
           {
             criando = true;
             objetoTemp = new RegularPoly(vClickI, qtdeLadosRegPoly,util.distancia(vInicial, vAtual)); 
           }
           else
           {
             criando = false;
             Objeto objeto = new RegularPoly(new Vertice(objetoTemp.pivot.x, objetoTemp.pivot.y), qtdeLadosRegPoly, ((RegularPoly)objetoTemp).raio);
             objetos.add(objeto);
             objetoTemp = null;
           }
         }
         else if(tipoCriacao == ModoEdicao.CRIAR_POLY)
         {
            if(!criando)
            {
              criando = true;
              objetoTemp = new Poly();
              Vertice v = new Vertice(mouseX, mouseY);
              
              ((Poly)objetoTemp).add(v);
            }
            else
            {
                Vertice v = new Vertice(mouseX, mouseY);
                
                if(util.distancia(v,((Poly)objetoTemp).vertices.get(0)) <= Poly.raioConclusaoCriacao && ((Poly)objetoTemp).vertices.size() > 2)
                {
                  criando = false;
                  ((Poly)objetoTemp).EncontrarPivot();
                  objetos.add(new Poly(((Poly)objetoTemp).vertices, ((Poly)objetoTemp).pivot));
                  
                  desenharTodosObjetos(true);
                  objetoTemp = null;
                  return;
                }
                else
                {
                  ((Poly)objetoTemp).add(v);
                  return;
                }
              
            }
         }
         else if(tipoCriacao == ModoEdicao.CRIAR_CIRCULO)
         {
           Vertice pivot = new Vertice(mouseX, mouseY);
           u.mapeamentoReverso(pivot);
           
           Objeto objeto = new Circulo(pivot, tamRaio);
           
           u.mapeamento(pivot);
           System.out.println("Pivot: " + pivot.x + " : " + pivot.y + " RAIO: " + tamRaio );
           objetos.add(objeto);
         }
         else if(tipoCriacao == ModoEdicao.CRIAR_LINHA)
         {
           Vertice v = new Vertice(mouseX, mouseY);
           
           if(!criando)
           {
             criando = true;
             objetoTemp = new Linha();
             ((Linha)objetoTemp).addVertice(v);
           }
           else
           {
             ((Linha)objetoTemp).vertices.add(v);
             criando = false;
             objetos.add(objetoTemp);
             objetoTemp = null;
             desenharTodosObjetos(true);
             return;
           }
           
         }
         
         break;
         
         case ModoEdicao.MODO_EDICAO_SELECIONAR:
         
         naoSelecionarTodos();
         
         for(int i = objetos.size() - 1; i >= 0; i--)
         {
           if(objetos.get(i).dentroPoligono(vClickI))
           {
              indiceObjSel = i;
              objetos.get(indiceObjSel).selecionar();
              break;
           }
         }
         
         break;
         
         case ModoEdicao.MODO_EDICAO_COLORIR:
         for(int i = 0; i < objetos.size(); i++)
         {
           if(objetos.get(i).dentroPoligono(vClickI))
           {
             objetos.get(i).setCorPreenchimento(corSelec);
             break;
           }
         }
         break;
     
     }
     
     desenharTodosObjetos(true);
}
  
void mouseMoved()
{
   vMouseAtual.x = mouseX;
   vMouseAtual.y = mouseY;
   
   int idBtn = botaoClickado(vMouseAtual);
  
   if(idBtn == -1)
   {
     switch(MODO_EDICAO.modoEdicao)
     {
       case ModoEdicao.MODO_EDICAO_TRANSFORMAR:
       break;
       
       case ModoEdicao.MODO_EDICAO_CRIAR:
         if(tipoCriacao == ModoEdicao.CRIAR_REGULAR_POLY)
         {
           if(criando)
           {
             Vertice vAtual = new Vertice(mouseX, mouseY);
             
             objetoTemp = new RegularPoly(vClickI, qtdeLadosRegPoly, util.distancia(vClickI, vAtual));
             desenharTodosObjetos(false);
             objetoTemp.desenhar(false);
             
           }
         }
         else if(tipoCriacao == ModoEdicao.CRIAR_POLY)
         {
           if(criando)
           {
             Vertice v = new Vertice(mouseX, mouseY);
             Vertice ultimo = ((Poly)objetoTemp).retornaUltimoVertice(); 
             desenharTodosObjetos(false);
             ((Poly)objetoTemp).desenharParc();
             linha((int)ultimo.x,(int)ultimo.y,(int)v.x,(int)v.y, objetoTemp.corBordaAtual);
             return;
           }
         }
         else if(tipoCriacao == ModoEdicao.CRIAR_LINHA)
         {
           if(criando)
           {
             Vertice v = new Vertice(mouseX, mouseY);
             ((Linha)objetoTemp).addVertice(1, v);
             desenharTodosObjetos(false);
             
             ((Linha)objetoTemp).desenhar(false);
             return;
           }
         }
         
       break;
     }
   }
   menu.DesenharMouseEnter(idBtn);
   
}

void mouseReleased()
{
  
  if(MODO_EDICAO.modoEdicao == ModoEdicao.MODO_EDICAO_TRANSFORMAR)
  {
     try
     {
       if(objetoTemp instanceof RegularPoly)
       {
         ((RegularPoly)objetoTemp).copiarTodosPontos(objetos.get(indiceObjSel));
       //objetos.get(indiceObjSel).desenhar();
       
         objetoTemp = null;
       }
       else if(objetoTemp instanceof Poly)
       {
         ((Poly)objetoTemp).copiarTodosPontos(objetos.get(indiceObjSel));
         
         objetoTemp = null;
       }
       else if(objetoTemp instanceof Circulo)
       {
         ((Circulo)objetoTemp).copiarTodosPontos(objetos.get(indiceObjSel));
         
         objetoTemp = null;
       }
       else if(objetoTemp instanceof Linha)
       {
         ((Linha)objetoTemp).copiarTodosPontos(objetos.get(indiceObjSel));
         
         objetoTemp = null;
       }
     }
     catch(Exception ex)
     {
        System.out.println("ERRO MOUSE RELEASE LEFT \n\n" + ex.getMessage());
     }
   }
   
    desenharTodosObjetos(true);
   
}

void mouseDragged()
{
  
  switch(MODO_EDICAO.modoEdicao)
  {
      case ModoEdicao.MODO_EDICAO_TRANSFORMAR:
         if(indiceObjSel < 0)
         {
           System.out.println("**********NENHUM OBJETO SELECIONADO**********");
           return;
         }
         if(objetos.get(indiceObjSel) instanceof RegularPoly)
         {
            if(objetoTemp == null)
            {
              RegularPoly objSel = (RegularPoly)objetos.get(indiceObjSel);
              
              objetoTemp = new RegularPoly(new Vertice(objSel.pivot.x, objSel.pivot.y), objSel.quantidadeLados, objSel.raio);
            }
         }
         else if(objetos.get(indiceObjSel) instanceof Poly)
         {
            if(objetoTemp == null)
            {
              Poly objSel = (Poly)objetos.get(indiceObjSel);
              
              objetoTemp = new Poly(objSel.vertices, objSel.pivot);
            }
         }
         else if(objetos.get(indiceObjSel) instanceof Circulo)
         {
           if(objetoTemp == null)
           {
             Circulo objSel = (Circulo)objetos.get(indiceObjSel);
             
             objetoTemp = new Circulo(new Vertice(objSel.pivot.x, objSel.pivot.y), objSel.raio);
           }
         }
         else if(objetos.get(indiceObjSel) instanceof Linha)
         {
           if(objetoTemp == null)
           {
               Linha objSel = (Linha)objetos.get(indiceObjSel);
               
               objetoTemp = new Linha(objSel.vertices);
           }
         }
      
        switch(mouseButton)
        { 
          case LEFT:
              if(objetoTemp instanceof RegularPoly)
              {
                try{
                  
                  ((RegularPoly)objetos.get(indiceObjSel)).copiarTodosPontos(objetoTemp);      
                  float deltaX = (mouseX - vClickI.x);
                  float deltaY = (mouseY - vClickI.y);
                  
                  //System.out.println("DELTAX: " + deltaX);
                  //System.out.println("DELTAY: " + deltaY);
                
                  ((RegularPoly)objetoTemp).transladar(deltaX, deltaY);
               
                  desenharObjetosNaoSelecionados();
                  objetoTemp.desenhar(false);
                }
                catch(Exception ex)
                {
                   System.out.println("ERRO MOUSE DRAG LEFT \n\n" + ex.getMessage());
                }
              }
              if(objetoTemp instanceof Poly)
              {
                try{
                  
                  ((Poly)objetos.get(indiceObjSel)).copiarTodosPontos(objetoTemp);
                  
                  float deltaX = (mouseX - vClickI.x);
                  float deltaY = (mouseY - vClickI.y);
                
                  ((Poly)objetoTemp).transladar(deltaX, deltaY);
               
                  desenharObjetosNaoSelecionados();
                  objetoTemp.desenhar(false);
                }
                catch(Exception ex)
                {
                   System.out.println("ERRO MOUSE DRAG LEFT \n\n" + ex.getMessage());
                }
              }
              if(objetoTemp instanceof Circulo)
              {
                try
                {
                  ((Circulo)objetos.get(indiceObjSel)).copiarTodosPontos(objetoTemp);
                  float deltaX = (mouseX - vClickI.x);
                  float deltaY = (mouseY - vClickI.y);
                  System.out.println("DELTAX: " + deltaX);
                  System.out.println("DELTAY: " + deltaY);
                  
                  ((Circulo)objetoTemp).transladar(deltaX, deltaY);
                  
                  desenharObjetosNaoSelecionados();
                  objetoTemp.desenhar(false);
                  
                }
                catch(Exception ex)
                {
                  System.out.println("ERRO MOUSE DRAG LEFT \n\n" + ex.getMessage());
                }
              }
              if(objetoTemp instanceof Linha)
              {
                  try{
                  
                  ((Linha)objetos.get(indiceObjSel)).copiarTodosPontos(objetoTemp);      
                  float deltaX = (mouseX - vClickI.x);
                  float deltaY = (mouseY - vClickI.y);
                  
                  //System.out.println("DELTAX: " + deltaX);
                  //System.out.println("DELTAY: " + deltaY);
                
                  ((Linha)objetoTemp).transladar(deltaX, deltaY);
               
                  desenharObjetosNaoSelecionados();
                  objetoTemp.desenhar(false);
                }
                catch(Exception ex)
                {
                   System.out.println("ERRO MOUSE DRAG LEFT \n\n" + ex.getMessage());
                }
              }
            break;
            
          case RIGHT:
            
            if(objetoTemp instanceof Linha)
            {
              try{
                
                ((Linha)objetos.get(indiceObjSel)).copiarTodosPontos(objetoTemp);
                objetoTemp.mapearRev(u);
              
                float sX = ((mouseX - vClickI.x) * 0.01) + 1;
                float sY = ((mouseY - vClickI.x) * 0.01) - 1;
              
                ((Linha)objetoTemp).escalaPivot(sX, -sY);
              
                objetoTemp.mapear(u);
              
                desenharObjetosNaoSelecionados();
                objetoTemp.desenhar(false);
              }
              catch(Exception ex)
              {
                 System.out.println("ERRO MOUSE DRAG RIGHT \n\n" + ex.getMessage());
              }
            }
          
            if(objetoTemp instanceof RegularPoly)
            {
              try{
                
                ((RegularPoly)objetos.get(indiceObjSel)).copiarTodosPontos(objetoTemp);
                objetoTemp.mapearRev(u);
              
                float sX = ((mouseX - vClickI.x) * 0.01) + 1;
                
                ((RegularPoly)objetoTemp).escalaPivot(sX, sX);
              
                objetoTemp.mapear(u);
              
                desenharObjetosNaoSelecionados();
                objetoTemp.desenhar(false);
              }
              catch(Exception ex)
              {
                 System.out.println("ERRO MOUSE DRAG RIGHT \n\n" + ex.getMessage());
              }
            }
            else if(objetoTemp instanceof Poly)
            {
              try{
                
                ((Poly)objetos.get(indiceObjSel)).copiarTodosPontos(objetoTemp);
                objetoTemp.mapearRev(u);
              
                float sX = ((mouseX - vClickI.x) * 0.01) + 1;
                float sY = ((mouseY - vClickI.x) * 0.01) - 1;
              
                ((Poly)objetoTemp).escalaPivot(sX, sX);
              
                objetoTemp.mapear(u);
              
                desenharObjetosNaoSelecionados();
                objetoTemp.desenhar(false);
              }
              catch(Exception ex)
              {
                 System.out.println("ERRO MOUSE DRAG RIGHT \n\n" + ex.getMessage());
              }
            }
            else if(objetoTemp instanceof Circulo)
            {
              try
              {
                  ((Circulo)objetos.get(indiceObjSel)).copiarTodosPontos(objetoTemp);
                  
                  objetoTemp.mapearRev(u);
              
                  float s = ((mouseX - vClickI.x) * 0.01) + 1;
                  
                  ((Circulo)objetoTemp).escala(s);
              
                  objetoTemp.mapear(u);
                  
                  desenharObjetosNaoSelecionados();
                  objetoTemp.desenhar(false);
              }
              catch(Exception ex)
              {
                System.out.println("ERRO MOUSE DRAG RIGHT \n\n" + ex.getMessage());
              }
            }
            break;
            
          case CENTER:
            if(objetoTemp instanceof Linha)
            {
              try{
                
                ((Linha)objetos.get(indiceObjSel)).copiarTodosPontos(objetoTemp);
                objetoTemp.mapearRev(u);
                
                float grausRad = util.distancia(vClickI, new Vertice(mouseX, mouseY));
                grausRad = (grausRad * PI) / 180;
                ((Linha)objetoTemp).rotacionarPivot(grausRad);
                objetoTemp.mapear(u);
               
                desenharObjetosNaoSelecionados();
                objetoTemp.desenhar(false);
              }
              catch(Exception ex)
              {
                System.out.println("ERRO MOUSE DRAG CENTER \n\n" + ex.getMessage());
              }
            }
            if(objetoTemp instanceof RegularPoly)
            {
              try{
                
                ((RegularPoly)objetos.get(indiceObjSel)).copiarTodosPontos(objetoTemp);
                objetoTemp.mapearRev(u);
                
                float grausRad = util.distancia(vClickI, new Vertice(mouseX, mouseY));
                grausRad = (grausRad * PI) / 180;
                ((RegularPoly)objetoTemp).rotacionarPivot(grausRad);
                objetoTemp.mapear(u);
               
                desenharObjetosNaoSelecionados();
                objetoTemp.desenhar(false);
              }
              catch(Exception ex)
              {
                System.out.println("ERRO MOUSE DRAG CENTER \n\n" + ex.getMessage());
              }
            }
            else if(objetoTemp instanceof Poly)
            {
              try{
                
                ((Poly)objetos.get(indiceObjSel)).copiarTodosPontos(objetoTemp);
                objetoTemp.mapearRev(u);
                
                float grausRad = util.distancia(vClickI, new Vertice(mouseX, mouseY));
                grausRad = (grausRad * PI) / 180;
                ((Poly)objetoTemp).rotacionarPivot(grausRad);
                objetoTemp.mapear(u);
               
                desenharObjetosNaoSelecionados();
                objetoTemp.desenhar(false);
              }
              catch(Exception ex)
              {
                System.out.println("ERRO MOUSE DRAG CENTER \n\n" + ex.getMessage());
              }
            }
            else if(objetoTemp instanceof Circulo)
            {
              try 
              {
                ((Circulo)objetos.get(indiceObjSel)).copiarTodosPontos(objetoTemp);
                objetoTemp.mapearRev(u);
                
                float grausRad = util.distancia(vClickI, new Vertice(mouseX, mouseY));
                grausRad = (grausRad * PI) / 180;
                ((Circulo)objetoTemp).rotacionar(grausRad);
                objetoTemp.mapear(u);
                
                desenharObjetosNaoSelecionados();
                objetoTemp.desenhar(false);
              }
              catch(Exception ex)
              {
                System.out.println("ERRO MOUSE DRAG CENTER \n\n" + ex.getMessage());
              }
              
            }
            
            
            break;
        }
        break;
  }
 
}

public void alterarEstadoNOVO()
{
   System.out.println("**********ModoEdicao: CRIAR NOVO**********");
   tipoCriacao = ModoEdicao.CRIAR_REGULAR_POLY;
   System.out.println("- MODO_CRIACAO: " + tipoCriacao);
   MODO_EDICAO.modoEdicao = ModoEdicao.MODO_EDICAO_CRIAR;
   menu.estadoBotoesNovo();
   
   if(indiceObjSel >= 0)
   {
      objetos.get(indiceObjSel).naoSelecionar();
      indiceObjSel = -1;
    }
    
    desenharTodosObjetos(true);
}

public void alterarEstadoSELECIONAR()
{
  System.out.println("**********ModoEdicao: SELECIONAR**********");
  MODO_EDICAO.modoEdicao = ModoEdicao.MODO_EDICAO_SELECIONAR;
  menu.estadoBotoesSelecionar();
}

public void alterarEstadoTRANSFORMAR()
{
  System.out.println("**********ModoEdicao: TRANSFORMAR**********");
  MODO_EDICAO.modoEdicao = ModoEdicao.MODO_EDICAO_TRANSFORMAR;
  menu.estadoBotoesTransformar();
  
  desenharTodosObjetos(true);
}

public void alterarEstadoCOLORIR()
{
  System.out.println("**********ModoEdicao: COLORIR**********");
  MODO_EDICAO.modoEdicao = ModoEdicao.MODO_EDICAO_COLORIR;
  menu.estadoBotoesColorir();
  
  //desenharTodosObjetos(true);
}

public void alterarEstadoDELETAR()
{
  System.out.println("**********ModoEdicao: DELETAR*********");
  //MODO_EDICAO.modoEdicao = ModoEdicao.MODO_EDICAO_DELETAR;
  menu.estadoBotoesDeletar();
  
  removerObjeto(indiceObjSel);
  
}

public void  alterarEstadoCIRCULO()
{
  if(MODO_EDICAO.modoEdicao == ModoEdicao.MODO_EDICAO_CRIAR)
  {
    tipoCriacao = ModoEdicao.CRIAR_CIRCULO;
    System.out.println("- MODO_CRIACAO: " + tipoCriacao);
  }
}

public void alterarEstadoREGULARPOLY()
{
  if(MODO_EDICAO.modoEdicao == ModoEdicao.MODO_EDICAO_CRIAR)
  {
    tipoCriacao = ModoEdicao.CRIAR_REGULAR_POLY;
    System.out.println("- MODO_CRIACAO: " + tipoCriacao);
  }
}

public void alterarEstadoPOLY()
{
  if(MODO_EDICAO.modoEdicao == ModoEdicao.MODO_EDICAO_CRIAR)
  {
    tipoCriacao = ModoEdicao.CRIAR_POLY;
    System.out.println("- MODO_CRIACAO: " + tipoCriacao);
  }
}

public void alterarEstadoLINHA()
{
  if(MODO_EDICAO.modoEdicao == ModoEdicao.MODO_EDICAO_CRIAR)
  {
    tipoCriacao = ModoEdicao.CRIAR_LINHA;
    System.out.println("- MODO_CRIACAO: " + tipoCriacao);
  }
}



void keyPressed()
{
  switch(key)
  {
    
    case 'q':
      apagarTodos();
      break;
    
    case 'n':
     alterarEstadoNOVO();
      break;
      
    case 's':
      alterarEstadoSELECIONAR();
      break;
      
    case 't':
      alterarEstadoTRANSFORMAR();
      break;
      
    case 'd':
      alterarEstadoDELETAR();
      break;
      
    case 'r':
      
      break;
      
    case 'p':
      
      break;
      
    case 'c':
      alterarEstadoCIRCULO();
    
    break;
      
    default:
      
      boolean numeroDigitado = ((int)key >= 48 && (int)key <= 57); 
      
      if(MODO_EDICAO.modoEdicao == ModoEdicao.MODO_EDICAO_CRIAR)
      {
        if(tipoCriacao == ModoEdicao.CRIAR_REGULAR_POLY && numeroDigitado)
        {
          int qtde = Integer.parseInt(key + "");
          if(qtde > 2)
          {
            qtdeLadosRegPoly = qtde; 
            System.out.println("Quantidade de lados: " + qtdeLadosRegPoly);
          }
          else
          {
            System.out.println("Quantidade inválida");
          }
        }
        if(tipoCriacao == ModoEdicao.CRIAR_CIRCULO && numeroDigitado)
        {
          
        }
      }
      else if(MODO_EDICAO.modoEdicao == ModoEdicao.MODO_EDICAO_SELECIONAR)
      {
          if(numeroDigitado)
          {
            int num = Integer.parseInt(key + "");
            
            if(num > objetos.size() || num < 0)
            {
              System.out.println("índice inválido");
              System.out.println("qtde obj :" + objetos.size());
            }
            else if(num > 0)
            {
              num--;
              
              if(indiceObjSel != -1)
              {
                objetos.get(indiceObjSel).naoSelecionar();
              }
              
              indiceObjSel = num;
              objetos.get(indiceObjSel).selecionar();
              System.out.println("índice selecionado: " + (indiceObjSel + 1));
            }
            else if(num == 0)
            {
              naoSelecionarTodos();
              
            }
            
            desenharTodosObjetos(true);
            
          }
      }
      break;
  }
}

void naoSelecionarTodos()
{
  for(int i = 0; i < objetos.size(); i++)
  {
    objetos.get(i).naoSelecionar();
  }
  
  indiceObjSel = -1;
}

void apagarTodos()
{
  background(255);
  objetos.clear();
  indiceObjSel = -1;
  desenharTodosObjetos(true);
}

void desenharTodosObjetos(boolean desenharMenu)
{
  background(255);
  stroke(200);
  line(0, height/2, width, height/2);
  line(width/2, 0, width/2, height);
  
  if(desenharMenu)
       menu.desenharBotoes();
  
  for(int i = 0; i < objetos.size(); i++)
  {
    objetos.get(i).desenhar(true);
  }
}

void desenharObjetosNaoSelecionados()
{
  background(255);
  stroke(200);
  line(0, height/2, width, height/2);
  line(width/2, 0, width/2, height);
  
 // menu.desenharBotoes();
  
  for(int i = 0; i < objetos.size(); i++)
  {
    if(i != indiceObjSel)
    {
      objetos.get(i).desenhar(true);
    }
  }
}

void removerObjeto(int indiceSelecionado)
{
  if(objetos.size() > 0 && indiceSelecionado >= 0)
  {
    objetos.remove(indiceSelecionado);
    
    this.indiceObjSel = -1;
  
    
    desenharTodosObjetos(true);
    
    System.out.println("objeto " + (indiceSelecionado + 1) + " removido");
  }
  
}

public int botaoClickado(Vertice vClick)
{
  for(int i = 0; i < menu.botoes.size(); i++)
  {
    if(!menu.botoes.get(i).visivel)
      continue;
    
    if(util.dentroPoligono(vClick, menu.botoes.get(i).vertices))
    {
      return menu.botoes.get(i).id;
    }
  }
  
  return -1;
}


  void linha(int xi,int yi, int xf, int yf, Cor cor)
 {
   
    int x = xi;
    int y = yi;
    int incX = 1;
    int incY = 1;
    float deltaX = xf - xi;
    float deltaY = yf - yi;
    float coefA = (deltaY / deltaX);
    float coefB = yi  - (coefA * xi);
   
    if(xi > xf)
    {
      incX = -1;
    }
    
    if(yi > yf)
    {
      incY = -1;
    }
    
    if(deltaX < 0)
    {
      deltaX = -deltaX;
    }
    
    if(deltaY < 0)
    {
      deltaY = -deltaY;
    }
    
    if(deltaX >= deltaY)
    {
      
      while(x != xf)
      {
      //   System.out.println("TESTE11");
         set(x,y, color(cor.R, cor.G, cor.B));
        
        x+= incX;
        
        float dy = (coefA * x) + coefB;
       
         dy = Math.abs(dy - y);
     
        if(dy >= 0.5)
        {
          y += incY;
        }
      }
    }
    else
    {
      while(y != yf)
      {
   //     System.out.println("TESTE11");
        set(x,y, color(cor.R, cor.G, cor.B));
       
        y += incY;
        
        float dx = (y - coefB)/ coefA;
        
        dx = Math.abs(dx - x);
        
        if(dx >= 0.5)
        {
          x+= incX;
        }
        
      }
    }
 }