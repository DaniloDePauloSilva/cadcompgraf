class Linha extends Objeto
{
  ArrayList<Vertice> vertices = new ArrayList<Vertice>();
  Transform transform = new Transform();
  Vertice pivot = new Vertice();
  
  public Linha()
  {
    
  } 
  
  public Linha(ArrayList<Vertice> vertices)
  {
    addTodosPontos(vertices);
    
    EncontrarPivot();
    
  }
  
  public Linha(Vertice vInicial, Vertice vFinal)
  {
     this.vertices.add(vInicial);
     this.vertices.add(vFinal);
     EncontrarPivot();
  }
  
  public void addVertice(Vertice v)
  {
    this.vertices.add(v);
  }
  
  public void addVertice(int indice, Vertice v)
  {
    this.vertices.add(indice, v);
    
    EncontrarPivot();
  }
  
   public void EncontrarPivot()
  {
    
    
    float mediaX = 0;
    float mediaY = 0;
    float somaX = 0;
    float somaY = 0;
    float total = vertices.size();
    
    for(int i = 0; i < 2; i++)
    {
      somaX += vertices.get(i).x;
      somaY += vertices.get(i).y;
    }
    
    mediaX = (somaX/2);
    mediaY = (somaY/2);
    
    //pivot.x = mediaX;
    //pivot.y = mediaY;
    
    pivot = new Vertice(mediaX, mediaY);
  }
  
  public void addTodosPontos(ArrayList<Vertice> vertices)
  {
    this.vertices = new ArrayList<Vertice>();
    
    for(int i = 0; i < vertices.size(); i++)
    {
      this.vertices.add(new Vertice(vertices.get(i).x, vertices.get(i).y));
    } 
  }
  
  void copiarTodosPontos(Objeto objAlvo) throws Exception
  {
    if(objAlvo instanceof Linha)
    {
      Linha alvo = (Linha)objAlvo;
      
      if(alvo.vertices.size() == this.vertices.size())
      {
        for(int i = 0; i < alvo.vertices.size(); i++)
        {
          alvo.vertices.get(i).x = this.vertices.get(i).x;
          alvo.vertices.get(i).y = this.vertices.get(i).y;
        }
      
        //EncontrarPivot();
        alvo.pivot.x = this.pivot.x;
        alvo.pivot.y = this.pivot.y;
      }
    }
    else
    {
      throw new Exception("Não é possível copiar objetos de tipos diferentes");
    }
  }
  
  public boolean dentroPoligono(Vertice v)
  {
    Vertice pontoNaLinha = new Vertice();
    Utils util = new Utils();
    
    int xi = (int)vertices.get(0).x;
    int yi = (int)vertices.get(0).y;
    int xf = (int)vertices.get(1).x;
    int yf = (int)vertices.get(1).y;
    
    int x = xi;
    int y = yi;
    int incX = 1;
    int incY = 1;
    float deltaX = xf - xi;
    float deltaY = yf - yi;
    float coefA = (deltaY / deltaX);
    float coefB = yi  - (coefA * xi);
   
    if(xi > xf)
    {
      incX = -1;
    }
    
    if(yi > yf)
    {
      incY = -1;
    }
    
    if(deltaX < 0)
    {
      deltaX = -deltaX;
    }
    
    if(deltaY < 0)
    {
      deltaY = -deltaY;
    }
    
    if(deltaX >= deltaY)
    {
      
      while(x != xf)
      {
      //   System.out.println("TESTE11");
         //set(x,y, color(cor.R, cor.G, cor.B));
        pontoNaLinha.x = x;
        pontoNaLinha.y = y;
        
        if(util.distancia(pontoNaLinha, v) < 20.0)
        {
          return true;
        }
        
        x+= incX;
        
        float dy = (coefA * x) + coefB;
       
         dy = Math.abs(dy - y);
     
        if(dy >= 0.5)
        {
          y += incY;
        }
      }
    }
    else
    {
      while(y != yf)
      {
        pontoNaLinha.x = x;
        pontoNaLinha.y = y;
        
        if(util.distancia(pontoNaLinha, v) < 20.0)
        {
          return true;
        }
       
        y += incY;
        
        float dx = (y - coefB)/ coefA;
        
        dx = Math.abs(dx - x);
        
        if(dx >= 0.5)
        {
          x+= incX;
        }
        
      }
    }
    
    
     return false;
  }
  
  void mapear(Universo u)
  {
      for(int i = 0; i < vertices.size(); i++)
      {
        u.mapeamento(vertices.get(i));
      }
      
      u.mapeamento(pivot);
  }
  
  void mapearRev(Universo u)
  {
      for(int i = 0; i < vertices.size(); i++)
      {
        u.mapeamentoReverso(vertices.get(i));
      }
      
      u.mapeamentoReverso(pivot);
  }
  
  public void desenhar(boolean colorir)
  {
      EncontrarPivot();
    
      linha((int)vertices.get(0).x, (int)vertices.get(0).y, (int)vertices.get(1).x, (int)vertices.get(1).y, corBordaAtual);
      
      
  }
  
   void escala(float sX, float sY)
  {
    for(int i = 0; i < vertices.size(); i++)
    {
      transform.escala(vertices.get(i) , sX, sY);
    }
    
    transform.escala(pivot, sX, sY);
  }
  
  void escalaPivot(float sX, float sY)
  {
    Vertice temp  = new Vertice(this.pivot.x, this.pivot.y);
    this.transladar(-temp.x, -temp.y);
    
    for(int i = 0; i < vertices.size(); i++)
    {
      transform.escala(vertices.get(i), sX, sY);
    }
    
    this.transladar(temp.x, temp.y);
  }
  
  void transladar(float deltaX, float deltaY)
  {
    for(int i = 0; i < vertices.size(); i++)
    {
      transform.transladar(vertices.get(i), deltaX, deltaY);
    }
    
    transform.transladar(pivot, deltaX, deltaY);
  }
  
  void rotacionar(float grausRad)
  {
    for(int i = 0; i < vertices.size(); i++)
    {
      transform.rotacionar(vertices.get(i), grausRad);
    }
    
    transform.rotacionar(pivot, grausRad);
  }
  
  void rotacionarPivot(float grausRad)
  {
    Vertice temp = new Vertice(this.pivot.x, this.pivot.y);
    
    this.transladar(-temp.x, -temp.y);
    
    for(int i = 0; i < vertices.size(); i++)
    {
      transform.rotacionar(vertices.get(i), grausRad);
    }
    
    this.transladar(temp.x, temp.y);
  }
  
}