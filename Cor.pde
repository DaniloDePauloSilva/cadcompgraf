class Cor
{
  public int R;
  public int G;
  public int B;
  
  public Cor(){}
  
  public Cor(int R, int G, int B)
  {
    this.R = R;
    this.G = G;
    this.B = B;
  }
 
  
 
  public void corIdBotao(int idBotao)
  {
    switch(idBotao)
    {
      case Botao.ID_BOTAO_VERMELHO:
      this.R = 255;
      this.G = 0;
      this.B = 0;
      break;
      
      case Botao.ID_BOTAO_VERDE:
      this.R = 0;
      this.G = 255;
      this.B = 0;
      break;
      
      case Botao.ID_BOTAO_AZUL:
      this.R = 0;
      this.G = 0;
      this.B = 255;
      break; 
      
      case Botao.ID_BOTAO_AMARELO:
      this.R = 255;
      this.G = 255;
      this.B = 0;
      break; 
      
      case Botao.ID_BOTAO_VERDEAGUA:
      this.R = 0;
      this.G = 255;
      this.B = 255;
      break;
      
      case Botao.ID_BOTAO_MARROM:
      this.R = 85;
      this.G = 34;
      this.B = 0;
      break; 
      
      case Botao.ID_BOTAO_ROSA:
      this.R = 255;
      this.G = 0;
      this.B = 255;
      break;
      
      case Botao.ID_BOTAO_LARANJA:
      this.R = 255;
      this.G = 120;
      this.B = 0;
      break; 
      
      case Botao.ID_BOTAO_BRANCO:
      this.R = 255;
      this.G = 255;
      this.B = 255;
      break; 
      
      case Botao.ID_BOTAO_CINZA:
      this.R = 125;
      this.G = 125;
      this.B = 125;
      break; 
      
      case Botao.ID_BOTAO_PRETO:
      this.R = 0;
      this.G = 0;
      this.B = 0;
      break; 
      
      case Botao.ID_BOTAO_MAGENTA:
      this.R = 139;
      this.G = 0;
      this.B = 139;
      break;
      
      case Botao.ID_BOTAO_SLATEBLUE:
      this.R = 106;
      this.G = 90;
      this.B = 205;
      break;
      
      case Botao.ID_BOTAO_DARKCYAN:
      this.R = 0;
      this.G = 139;
      this.B = 139;
      break;
      
      case Botao.ID_BOTAO_HOTPINK:
      this.R = 255;
      this.G = 105;
      this.B = 180;
      break;
      
      case Botao.ID_BOTAO_SPRINGGREENS:
      this.R = 0;
      this.G = 255;
      this.B = 127;
      break;
      
    }
    
  }
  
  
}