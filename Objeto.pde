abstract class Objeto
{
  Matriz m;
  Vertice pivot;
  Cor corBordaAtual;
  Cor corBordaSelecionado = new Cor(255, 0, 0);
  Cor cor;
  public Cor corPreenchimento = new Cor(255,255,255);

  boolean selecionado = false;
  
  
  
  public Objeto(Vertice pivot,Cor cor)
  {
    m = new Matriz(3);
    this.pivot = pivot;
    this.cor = cor;
    this.corBordaAtual = cor;
    
  }
  
  public void selecionar()
  {
    this.selecionado = true;
    corBordaAtual = corBordaSelecionado;
  }
  
  public void naoSelecionar()
  {
    this.selecionado = false;
    corBordaAtual = cor;
  }
  
  public Objeto(Vertice pivot)
  {
    m = new Matriz(3);
    this.pivot = pivot;
    this.cor = new Cor();
    this.corBordaAtual = cor;
  }
  
  public Objeto()
  {
    pivot = new Vertice();
    this.cor = new Cor();
    corBordaAtual = cor;
  }
  
  public void setCorPreenchimento(Cor cor)
  {
    this.corPreenchimento = cor;
  }
  
  public abstract boolean dentroPoligono(Vertice v);
  
  public abstract void mapear(Universo u);
  
  public abstract void mapearRev(Universo u);
  
  public abstract void desenhar(boolean colorir);
  
  
  
}