class Menu
{

  public static final int MOUSE_ENTER = 0;
  public static final int MOUSE_LEAVE = 1;
  public static final int MOUSE_PRESSED = 2;
  public static final int MOUSE_RELEASE = 3;
  
  ArrayList<Botao> botoes = new ArrayList<Botao>();
  Utils util = new Utils();
  
  public static final float alturaPadrao = 30;
  public static final float larguraPadrao = 100;
  
  public static final float alturaCor = 20;
  public static final float larguraCor = 20;
  
  public int altura = height;
  public int largura = 150;
  
  //TESTE===========
  //public int altura = 30;
  //public int largura = width;
  
  //========
  
  public Menu()
  {
    botoes.add(new Botao(Botao.ID_BOTAO_NOVO,"NOVO",new Vertice(0,0), larguraPadrao, alturaPadrao, true));
    botoes.add(new Botao(Botao.ID_BOTAO_TRANSFORMAR,"TRANSFORMAR", new Vertice(0,alturaPadrao + 1), larguraPadrao, alturaPadrao, true));
    botoes.add(new Botao(Botao.ID_BOTAO_DELETAR,"DELETAR", new Vertice(0, 2 * alturaPadrao + 1), larguraPadrao, alturaPadrao, true));
    botoes.add(new Botao(Botao.ID_BOTAO_SELECIONAR,"SELECIONAR", new Vertice(0,3 * alturaPadrao + 1), larguraPadrao, alturaPadrao, true));
    botoes.add(new Botao(Botao.ID_BOTAO_COLORIR,"COLORIR", new Vertice(0,4 * alturaPadrao + 1), larguraPadrao, alturaPadrao, true));
    
    botoes.add(new Botao(Botao.ID_BOTAO_CIRCULO, "Circulo", new Vertice(0, 5 * alturaPadrao + 20), larguraPadrao, alturaPadrao, false));
    botoes.add(new Botao(Botao.ID_BOTAO_REGULARPOLY, "Poligono Regular", new Vertice(0, 6 * alturaPadrao + 20), larguraPadrao, alturaPadrao, false));
    botoes.add(new Botao(Botao.ID_BOTAO_POLY, "Poligono", new Vertice(0, 7 * alturaPadrao + 20), larguraPadrao, alturaPadrao, false));
    botoes.add(new Botao(Botao.ID_BOTAO_LINHA, "Linha", new Vertice(0, 8 * alturaPadrao + 20), larguraPadrao, alturaPadrao, false));
    
    botoes.add(new Botao(Botao.ID_BOTAO_VERMELHO, "", new Vertice(0, 5 * alturaCor + 100), larguraCor, alturaCor, false, new Cor()));
    botoes.add(new Botao(Botao.ID_BOTAO_LARANJA, "", new Vertice(0, 6 * alturaCor + 100), larguraCor, alturaCor, false,new Cor()));
    botoes.add(new Botao(Botao.ID_BOTAO_AMARELO, "", new Vertice(0, 7 * alturaCor + 100), larguraCor, alturaCor, false, new Cor()));
    botoes.add(new Botao(Botao.ID_BOTAO_VERDE, "", new Vertice(0, 8 * alturaCor + 100), larguraCor, alturaCor, false,new Cor()));
    
    botoes.add(new Botao(Botao.ID_BOTAO_ROSA, "", new Vertice(larguraCor, 5 * alturaCor + 100), larguraCor, alturaCor, false,new Cor()));
    botoes.add(new Botao(Botao.ID_BOTAO_MARROM, "", new Vertice(larguraCor, 6 * alturaCor + 100), larguraCor, alturaCor, false, new Cor()));
    botoes.add(new Botao(Botao.ID_BOTAO_VERDEAGUA,"", new Vertice(larguraCor, 7 * alturaCor + 100), larguraCor, alturaCor,false, new Cor()));
    botoes.add(new Botao(Botao.ID_BOTAO_AZUL, "", new Vertice(larguraCor, 8 * alturaCor + 100), larguraCor, alturaCor, false, new Cor()));
    
    botoes.add(new Botao(Botao.ID_BOTAO_PRETO, "", new Vertice(larguraCor * 2, 5 * alturaCor + 100), larguraCor, alturaCor, false,new Cor()));
    botoes.add(new Botao(Botao.ID_BOTAO_BRANCO, "", new Vertice(larguraCor * 2, 6 * alturaCor + 100), larguraCor, alturaCor, false, new Cor()));
    botoes.add(new Botao(Botao.ID_BOTAO_CINZA, "", new Vertice(larguraCor * 2, 7 * alturaCor + 100), larguraCor, alturaCor, false, new Cor()));
    botoes.add(new Botao(Botao.ID_BOTAO_MAGENTA, "", new Vertice(larguraCor * 2, 8 * alturaCor + 100), larguraCor, alturaCor, false, new Cor()));
    
    botoes.add(new Botao(Botao.ID_BOTAO_SLATEBLUE, "", new Vertice(larguraCor * 3, 5 * alturaCor + 100), larguraCor, alturaCor, false, new Cor()));
    botoes.add(new Botao(Botao.ID_BOTAO_DARKCYAN, "", new Vertice(larguraCor * 3, 6 * alturaCor + 100), larguraCor, alturaCor, false, new Cor()));
    botoes.add(new Botao(Botao.ID_BOTAO_HOTPINK, "", new Vertice(larguraCor * 3, 7 * alturaCor + 100), larguraCor, alturaCor, false, new Cor()));
    botoes.add(new Botao(Botao.ID_BOTAO_SPRINGGREENS, "", new Vertice(larguraCor * 3, 8 * alturaCor + 100), larguraCor, alturaCor, false, new Cor()));
    
    
    botoes.add(new Botao(Botao.ID_BOTAO_CORSELECIONADA, "", new Vertice(0, 4 * alturaCor + 85), larguraCor + 10, alturaCor + 10, false, new Cor()));
  
    
    
    //+++++++++++++++++++++++TESTE+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //botoes.add(new Botao(Botao.ID_BOTAO_NOVO,"NOVO",new Vertice(0,0), larguraPadrao, alturaPadrao, true));
    //botoes.add(new Botao(Botao.ID_BOTAO_TRANSFORMAR,"TRANSFORMAR", new Vertice(larguraPadrao + 1, 0), larguraPadrao, alturaPadrao, true));
    //botoes.add(new Botao(Botao.ID_BOTAO_DELETAR,"DELETAR", new Vertice(2 * larguraPadrao + 2, 0), larguraPadrao, alturaPadrao, true));
    //botoes.add(new Botao(Botao.ID_BOTAO_SELECIONAR,"SELECIONAR", new Vertice(3 * larguraPadrao + 3, 0), larguraPadrao, alturaPadrao, true));
    
    //botoes.add(new Botao(Botao.ID_BOTAO_CIRCULO, "Circulo", new Vertice(4 * larguraPadrao + 20,0), larguraPadrao, alturaPadrao, false));
    //botoes.add(new Botao(Botao.ID_BOTAO_REGULARPOLY, "Poligono Regular", new Vertice(5 * larguraPadrao + 20,0), larguraPadrao, alturaPadrao, false));
    //botoes.add(new Botao(Botao.ID_BOTAO_POLY, "Poligono", new Vertice(6 * larguraPadrao + 20,0), larguraPadrao, alturaPadrao, false));
    //botoes.add(new Botao(Botao.ID_BOTAO_LINHA, "Linha", new Vertice(7 * larguraPadrao + 20, 0), larguraPadrao, alturaPadrao, false));
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    
  }
  
  public void desenharBotoes()
  { 
    ArrayList<Botao> btnVisiveis = obtemBtnVisiveis(); 
    colorir(btnVisiveis);
    contornoETexto(btnVisiveis);
   
  }
  
  public void corDefault()
  {
    for(int i = 0; i < botoes.size(); i++)
    {
      botoes.get(i).corAtual = botoes.get(i).corFundo;
    }
  }
  
  public void estadoBotoesSelecionar()
  {
    corDefault();
    
    botoes.get(3).corAtual = botoes.get(3).corSelecionado;
   
    botoes.get(5).visivel = false;
    botoes.get(6).visivel = false;
    botoes.get(7).visivel = false;
    botoes.get(8).visivel = false;
   
    botoes.get(9).visivel = false;
    botoes.get(10).visivel = false;
    botoes.get(11).visivel = false;
    botoes.get(12).visivel = false;
    
    
    botoes.get(13).visivel = false;
    botoes.get(14).visivel = false;
    botoes.get(15).visivel = false;
    botoes.get(16).visivel = false;
    botoes.get(17).visivel = false;
    botoes.get(18).visivel = false;
    botoes.get(19).visivel = false;
    botoes.get(20).visivel = false;
    botoes.get(21).visivel = false;
    botoes.get(22).visivel = false;
    botoes.get(23).visivel = false;
    botoes.get(24).visivel = false;
    botoes.get(25).visivel = false;
  }
  
  public void estadoBotoesDeletar()
  {
    corDefault();
    
    botoes.get(2).corAtual = botoes.get(2).corSelecionado;
    
    botoes.get(5).visivel = false;
    botoes.get(6).visivel = false;
    botoes.get(7).visivel = false;
    botoes.get(8).visivel = false;
    
     botoes.get(9).visivel = false;
    botoes.get(10).visivel = false;
    botoes.get(11).visivel = false;
    botoes.get(12).visivel = false;
    
    
    botoes.get(13).visivel = false;
    botoes.get(14).visivel = false;
    botoes.get(15).visivel = false;
    botoes.get(16).visivel = false;
    botoes.get(17).visivel = false;
    botoes.get(18).visivel = false;
    botoes.get(19).visivel = false;
    botoes.get(20).visivel = false;
     botoes.get(21).visivel = false;
     botoes.get(22).visivel = false;
     botoes.get(23).visivel = false;
     botoes.get(24).visivel = false;
     botoes.get(25).visivel = false;
  }
  
  public void estadoBotoesTransformar()
  {
    corDefault();
    
    botoes.get(1).corAtual = botoes.get(1).corSelecionado;
    
    botoes.get(5).visivel = false;
    botoes.get(6).visivel = false;
    botoes.get(7).visivel = false;
    botoes.get(8).visivel = false;
    
    botoes.get(9).visivel = false;
    botoes.get(10).visivel = false;
    botoes.get(11).visivel = false;
    botoes.get(12).visivel = false;
    
    
    botoes.get(13).visivel = false;
    botoes.get(14).visivel = false;
    botoes.get(15).visivel = false;
    botoes.get(16).visivel = false;
    botoes.get(17).visivel = false;
    botoes.get(18).visivel = false;
    botoes.get(19).visivel = false;
    botoes.get(20).visivel = false;
     botoes.get(21).visivel = false;
     botoes.get(22).visivel = false;
     botoes.get(23).visivel = false;
     botoes.get(24).visivel = false;
     botoes.get(25).visivel = false;
  }
  
  public void estadoBotoesNovo()
  {
    corDefault();
    botoes.get(0).corAtual = botoes.get(0).corSelecionado;
    
    
    botoes.get(5).visivel = true;
    botoes.get(6).visivel = true;
    botoes.get(7).visivel = true;
    botoes.get(8).visivel = true;
    
    botoes.get(9).visivel = false;
    botoes.get(10).visivel = false;
    botoes.get(11).visivel = false;
    botoes.get(12).visivel = false;
    
    
    botoes.get(13).visivel = false;
    botoes.get(14).visivel = false;
    botoes.get(15).visivel = false;
    botoes.get(16).visivel = false;
    botoes.get(17).visivel = false;
    botoes.get(18).visivel = false;
    botoes.get(19).visivel = false;
    botoes.get(20).visivel = false;
     botoes.get(21).visivel = false;
     botoes.get(22).visivel = false;
     botoes.get(23).visivel = false;
     botoes.get(24).visivel = false;
     botoes.get(25).visivel = false;
  }
  
  public void estadoBotoesColorir()
  {
    corDefault();
     
    Botao btnColorir = retornaBotaoPorId(Botao.ID_BOTAO_COLORIR);
    btnColorir.corAtual = btnColorir.corSelecionado;
   
    botoes.get(5).visivel = false;
    botoes.get(6).visivel = false;
    botoes.get(7).visivel = false;
    botoes.get(8).visivel = false;
    
    botoes.get(9).visivel = true;
    botoes.get(10).visivel = true;
    botoes.get(11).visivel = true;
    botoes.get(12).visivel = true;
    
    botoes.get(13).visivel = true;
    botoes.get(14).visivel = true;
    botoes.get(15).visivel = true;
    botoes.get(16).visivel = true;
    botoes.get(17).visivel = true;
    botoes.get(18).visivel = true;
    botoes.get(19).visivel = true;
    botoes.get(20).visivel = true;
     botoes.get(21).visivel = true;
     botoes.get(22).visivel = true;
     botoes.get(23).visivel = true;
     botoes.get(24).visivel = true;
     botoes.get(25).visivel = true;
    
  }
  
  public boolean btnCor(int idBtn)
  {
    if(idBtn >= 9 && idBtn <= 25)
      return true;
      
    return false;
  }
  
  public void alterarCorSelecionada(Cor cor)
  {
    Botao btn = retornaBotaoPorId(Botao.ID_BOTAO_CORSELECIONADA);
    
    btn.corAtual = cor;
  }
  
  public void selecionarCor(int idBtnCorSelecionado)
  {
      Botao btn = retornaBotaoPorId(idBtnCorSelecionado);
      btn.corBordaAtual = btn.corSelecionado;
  }
  
  public ArrayList<Botao> obtemBtnVisiveis()
  {
    ArrayList<Botao> btnVisiveis = new ArrayList<Botao>();
    
    for(int i = 0; i < botoes.size(); i++)
    {
      if(botoes.get(i).visivel)
        btnVisiveis.add(botoes.get(i));    
    }
    
    return btnVisiveis;
  }
  
  public Botao retornaBotaoPorId(int idBtn)
  {
    for(int i = 0; i < botoes.size(); i++)
    {
      if(idBtn == botoes.get(i).id)
      {
        return botoes.get(i);
      }
    }
    
    return null;
  }
  
  public void alterarCorBotao(int idBtn, int idEvento)
  {
     Botao btn = retornaBotaoPorId(idBtn);
     if(!btn.visivel)
     {
       return;
     }
     Vertice aux = new Vertice();
    
     Cor cor;
     
     switch(idEvento)
     {
       case MOUSE_ENTER:
       cor = btn.corMouseEnter;
       break;
       
       case MOUSE_LEAVE:
       cor = btn.corFundo;
       break;
       
       case MOUSE_PRESSED:
       cor = btn.corMousePressed;
       break;
       
       case MOUSE_RELEASE:
       cor = btn.corSelecionado;
       break;
       
       default:
       cor = btn.corFundo;
       break;
     }
     
     for(int i = 0; i < larguraPadrao; i++)
     {
       for(int j = 0; j < height/2; j++)
       {
          aux.x = i;
          aux.y = j;
           
          if(util.dentroPoligono(aux, btn.vertices))
          {
            set(i,j, color(cor.R, cor.G, cor.B));
          }
       }
     }
  }
  
  
  
  public void colorir(ArrayList<Botao> btnVisiveis)
  {
     Vertice aux = new Vertice();
    
    for(int i = 1; i < larguraPadrao; i++)
    {
      for(int j = 1; j < height/2 ; j++)
      {
         aux.x = i;
         aux.y = j;
                 
         for(int k = 0; k < btnVisiveis.size(); k++)
         {
           if(util.dentroPoligono(aux, btnVisiveis.get(k).vertices))
           {
              set(i,j,color(btnVisiveis.get(k).corAtual.R,  btnVisiveis.get(k).corAtual.G, btnVisiveis.get(k).corAtual.B));              
           }
         }
      }
    }
  }
   
  public void colorir()
  {
    Vertice aux = new Vertice();
    
    for(int i = 1; i < larguraPadrao; i++)
    {
      for(int j = 1; j < height/2; j++)
      {
         aux.x = i;
         aux.y = j;
                 
         for(int k = 0; k < botoes.size(); k++)
         {
           if(util.dentroPoligono(aux, botoes.get(k).vertices))
           {
              set(i,j,color(botoes.get(k).corAtual.R, botoes.get(k).corAtual.G,botoes.get(k).corAtual.B));              
           }
         }
      }
    }
  }
  
  public void DesenharMousePressed(int idBtnSelecionado)
  {
    Vertice aux = new Vertice();
    ArrayList<Botao> btnVisiveis = obtemBtnVisiveis();
    
    for(int i = 0; i < larguraPadrao; i++)
    {
      for(int j = 0; j < height/2; j++)
      {
        aux.x = i;
        aux.y = j;
        
        for(int k = 0; k < btnVisiveis.size(); k++)
        {
          if(util.dentroPoligono(aux, btnVisiveis.get(k).vertices))
          {
            if(btnVisiveis.get(k).id == idBtnSelecionado)
              set(i,j, color(btnVisiveis.get(k).corMousePressed.R,btnVisiveis.get(k).corMousePressed.G,btnVisiveis.get(k).corMousePressed.B));
            else
              set(i,j, color(btnVisiveis.get(k).corAtual.R,btnVisiveis.get(k).corAtual.G,btnVisiveis.get(k).corAtual.B));
          
          }
        }
      }
    }
    
    contornoETexto(btnVisiveis);
  }
  
  public void DesenharMouseEnter(int idBtnSelecionado)
  {
    Vertice aux = new Vertice();
    ArrayList<Botao> btnVisiveis = obtemBtnVisiveis(); 
    
    for(int i = 1; i < larguraPadrao; i++)
    {
      for(int j = 1; j < height/2 ; j++)
      {
         aux.x = i;
         aux.y = j;
                 
         for(int k = 0; k < btnVisiveis.size(); k++)
         {
           if(util.dentroPoligono(aux, btnVisiveis.get(k).vertices))
           {
              if(btnVisiveis.get(k).id == idBtnSelecionado)
                set(i,j,color(btnVisiveis.get(k).corMouseEnter.R, btnVisiveis.get(k).corMouseEnter.G,btnVisiveis.get(k).corMouseEnter.B));
              else
                set(i,j,color(btnVisiveis.get(k).corAtual.R, btnVisiveis.get(k).corAtual.G,btnVisiveis.get(k).corAtual.B));
           }
         }
      }
    }
    
    contornoETexto(btnVisiveis);
    
  }
  
  public void contornoETexto(ArrayList<Botao> btnVisiveis)
  {
    PFont f = createFont("Arial", 10, true);
    textFont(f);
    textAlign(CENTER);
    
    for(int i = 0; i < btnVisiveis.size(); i++)
    {
       btnVisiveis.get(i).desenhar();
       fill(btnVisiveis.get(i).corTexto.R,  btnVisiveis.get(i).corTexto.G,  btnVisiveis.get(i).corTexto.B);
       text( btnVisiveis.get(i).texto,  btnVisiveis.get(i).pivot.x,  btnVisiveis.get(i).pivot.y);
    }
  }
  
}