class Circulo extends Objeto
{
  float raio;
  Transform transform = new Transform();
  Utils util = new Utils();
  
  public Circulo(Vertice pivot, float raio)
  {
    super(pivot);
    this.raio = raio;
  }
  
  public void desenhar(boolean colorir)
  {
    if(colorir)
      colorir();
      
      desenhar();
  }
  
  public void desenhar()
  {
     int x = 0; 
     int y = (int)raio;
     int u = 1;
     int v = (int)(2 * raio - 1);
     int e = 0;
     
     while(x < y)
     {
       set((int)pivot.x + x,(int)pivot.y + y, color(corBordaAtual.R, corBordaAtual.G, corBordaAtual.B));
       set((int)pivot.x + y,(int)pivot.y - x, color(corBordaAtual.R, corBordaAtual.G, corBordaAtual.B));
       set((int)pivot.x - x,(int)pivot.y - y, color(corBordaAtual.R, corBordaAtual.G, corBordaAtual.B));
       set((int)pivot.x - y,(int)pivot.y + x, color(corBordaAtual.R, corBordaAtual.G, corBordaAtual.B));
       
       x++;
       e += u;
       u += 2;
       
       if(v < 2 * e)
       {
         y--; 
         e -= v;
         v -= 2;
       }
       
       if(x > y)
       {
         break;
       }
       
       set((int)pivot.x + y,(int)pivot.y + x, color(corBordaAtual.R, corBordaAtual.G, corBordaAtual.B));
       set((int)pivot.x + x,(int)pivot.y - y, color(corBordaAtual.R, corBordaAtual.G, corBordaAtual.B));
       set((int)pivot.x - y,(int)pivot.y - x, color(corBordaAtual.R, corBordaAtual.G, corBordaAtual.B));
       set((int)pivot.x - x,(int)pivot.y + y, color(corBordaAtual.R, corBordaAtual.G, corBordaAtual.B));
       
     }
     
    linha((int)pivot.x,(int) pivot.y - 3,(int) pivot.x,(int) pivot.y +3 , corBordaAtual);
    linha((int)pivot.x - 3,(int) pivot.y,(int) pivot.x + 3,(int) pivot.y, corBordaAtual);
     
  }
  
  public void copiarTodosPontos(Objeto objAlvo) throws Exception
  {
     if(objAlvo instanceof Circulo)
     {
       Circulo alvo = (Circulo)objAlvo;
       
       alvo.pivot.x = this.pivot.x;
       alvo.pivot.y = this.pivot.y;
       
       alvo.raio = this.raio;
     }
     else
     {
       throw new Exception("Não é possível copiar objetos de tipos diferentes");
     }
  }
  
  public void transladar(float deltaX, float deltaY)
  {
   transform.transladar(pivot, deltaX, deltaY);
  }
  
  public void escala(float s)
  {
    //Vertice auxPivot = new Vertice(this.pivot.x, this.pivot.y);
    //this.transladar(-auxPivot.x, -auxPivot.y);
    //Vertice aux = new Vertice(pivot.x + raio, 0);
    //transform.transladar(aux, s, 0);
    //this.transladar(auxPivot.x, auxPivot.y);
    //raio = aux.x;
    
    Vertice auxPivot = new Vertice(this.pivot.x, this.pivot.y);
    this.transladar(-auxPivot.x, -auxPivot.y);
    Vertice aux = new Vertice(pivot.x + raio, 0);
    transform.escala(aux, s, 1);
    raio = aux.x;
    
    this.transladar(auxPivot.x, auxPivot.y);
  }
  
  public void rotacionar(float grausRad)
  {
    transform.rotacionar(pivot, grausRad);
  }
  
  public void mapear(Universo u)
  {
    Vertice auxRaio = new Vertice(pivot.x + raio, 0);
    u.mapeamento(auxRaio);
    u.mapeamento(pivot);
    
    raio = abs(auxRaio.x - pivot.x);
   
    auxRaio = null;
  }
  
  public void mapearRev(Universo u)
  {
    Vertice auxRaio = new Vertice(pivot.x + raio, 0);
    
    u.mapeamentoReverso(auxRaio);
    u.mapeamentoReverso(pivot);
    
    raio = abs(auxRaio.x - pivot.x);
    
  }
  
  public boolean dentroPoligono(Vertice v)
  {
    if(util.distancia(pivot, v) <= raio)
      return true;
    else
      return false;
      
  }
  
  public void colorir()
  {
    int menorX = (int)(pivot.x - raio);
    int menorY = (int)(pivot.y - raio);
    int maiorX = (int)(pivot.x + raio);
    int maiorY = (int)(pivot.y + raio);
    
    Vertice v = new Vertice();
    
    for(int i = menorX; i <= maiorX; i++)
    {
      for(int j = menorY; j <= maiorY; j++)
      {
        v.x = i;
        v.y = j;
        
        if(dentroPoligono(v))
        {
          set(i,j, color(this.corPreenchimento.R,this.corPreenchimento.G,this.corPreenchimento.B));
        }
      }
    }
  }
  
  
}