class ModoEdicao
{
  public static final int MODO_EDICAO_CRIAR = 0;
  public static final int MODO_EDICAO_TRANSFORMAR = 1;
  public static final int MODO_EDICAO_DELETAR = 2;
  public static final int MODO_EDICAO_SELECIONAR = 3;
  public static final int MODO_EDICAO_COLORIR = 4;
  
  public static final int CRIAR_REGULAR_POLY = 999;
  public static final int CRIAR_POLY = 998;
  public static final int CRIAR_CIRCULO = 997;
  public static final int CRIAR_LINHA = 996;
  
  public int modoEdicao;
  
}